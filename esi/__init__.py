from __future__ import unicode_literals

default_app_config = 'esi.apps.EsiConfig'

__version__ = '2.1.1'
__title__ = 'django-esi'
